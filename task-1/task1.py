import time

import numpy as np
from scipy.spatial import KDTree
from typing import List, Tuple, Dict, Set


class Rectangle:

    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.points = []

    def expand(self, cnt=-1):
        if cnt == -1:

            for i in range(self.x1, self.x2 + 1):
                self.points.append((i, self.y1))
                self.points.append((i, self.y2))

            for i in range(self.y1 + 1, self.y2):
                self.points.append((self.x1, i))
                self.points.append((self.x2, i))

            return

        self.points.append((self.x1, self.y1))
        self.points.append((self.x2, self.y2))

        for i in range(1, cnt + 1):
            self.points.append((self.x1 + (self.x2 - self.x1) * i / (cnt + 1), self.y1))
            self.points.append((self.x1 + (self.x2 - self.x1) * i / (cnt + 1), self.y2))

        self.points.append((self.x2, self.y1))

        for i in range(1, cnt + 1):
            self.points.append((self.x1, self.y1 + (self.y2 - self.y1) * i / (cnt + 1)))
            self.points.append((self.x2, self.y1 + (self.y2 - self.y1) * i / (cnt + 1)))

        self.points.append((self.x1, self.y2))

        return

    def get_points(self):
        return self.points

    def __str__(self):
        return f"({self.x1}, {self.y1}) - ({self.x2}, {self.y2})"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.x1 == other.x1 and self.x2 == other.x2 and self.y1 == other.y1 and self.y2 == other.y2

    def __hash__(self):
        return hash((self.x1, self.x2, self.y1, self.y2))

    def __lt__(self, other):
        return self.y1 < other.y1 or (self.y1 == other.y1 and self.x1 < other.x1)

    def __gt__(self, other):
        return self.y1 > other.y1 or (self.y1 == other.y1 and self.x1 > other.x1)

    def __le__(self, other):
        return self.y1 <= other.y1 or (self.y1 == other.y1 and self.x1 <= other.x1)

    def __ge__(self, other):
        return self.y1 >= other.y1 or (self.y1 == other.y1 and self.x1 >= other.x1)

    def __ne__(self, other):
        return not self.__eq__(other)


def load_points(file_name: str):
    with open(file_name, 'r') as f:
        data = f.readlines()
    data = [x.strip() for x in data]
    data = [x.split(" ") for x in data]
    data = [[int(x) for x in y] for y in data]
    return data


def create_rectangle(points: List, extra_points=-1) -> Rectangle:
    if len(points) != 4:
        print("Invalid number of points")
        exit(1)

    x1, y1, x2, y2 = points

    ret: Rectangle = Rectangle(x1, y1, x2, y2)
    ret.expand(extra_points)

    return ret


print("Loading points...")
data: List[List[int]] = load_points("points.txt")

extra_points: int = 13

print("Creating rectangles...")
rectangles: List[Rectangle] = []
for L in data:
    rectangles.append(create_rectangle(L, extra_points))

rectangles.sort(key=lambda x: (x.points[0][0], x.points[0][1]), reverse=True)

seed = "X-sorted-reversed"


def create_tree(rects: List[Rectangle]) -> Tuple[KDTree, Dict[int, Rectangle]]:
    identifiers: Dict[int, Rectangle] = {}
    points = []
    index = 0
    for r in rects:
        for p in r.get_points():
            points.append(p)
            identifiers[index] = r
            index += 1
    return KDTree(points), identifiers


def get_precomputed_angles(angle_increment: int) -> List[Tuple[float, float]]:
    angles: List[Tuple[float, float]] = []
    for i in range(1, angle_increment + 1):
        angles.append((np.cos(i * 360 / angle_increment) * 1.0, np.sin(i * 360 / angle_increment) * 1.0))
    return angles


tree, ids = create_tree(rectangles)
print("Tree created")

angles: List[Tuple[float, float]] = get_precomputed_angles(360)


def find_best(rect: Rectangle, tree: KDTree, ids: Dict[int, Rectangle], angles: List[Tuple[float, float]], radius: int,
              covered_global: Set[Rectangle]) -> Tuple[Tuple[float, float], Set[Rectangle]]:
    ret: Tuple[float, float] = (0, 0)
    covered_rects: Set[Rectangle] = set()

    smaller_radius = radius - 0.000001

    for (x, y) in rect.get_points():
        for (acos, asin) in angles:
            circle_center = (x + acos * smaller_radius, y + asin * smaller_radius)
            points_in_circle = tree.query_ball_point(circle_center, radius)
            segments: Set[Rectangle] = set()
            for point in points_in_circle:
                if ids[point] not in covered_global:
                    segments.add(ids[point])
            if len(segments) > len(covered_rects):
                covered_rects = segments
                ret = circle_center

    return ret, covered_rects


def algorithm(rectangles: List[Rectangle], tree: KDTree, ids: Dict[int, Rectangle], angles: List[Tuple[float, float]],
              radius: int) -> List[Tuple[float, float]]:
    ret: List[Tuple[float, float]] = []
    covered: Set[Rectangle] = set()

    for i in range(len(rectangles)):

        if i % 1000 == 0:
            print(i)

        if rectangles[i] in covered:
            continue

        best_point, covered_rects = find_best(rectangles[i], tree, ids, angles, radius, covered)

        if len(covered_rects) == 0:
            continue

        lent = len(covered)

        covered = covered.union(covered_rects)

        if len(covered) == lent:
            continue

        ret.append(best_point)

    return ret


def plot_results(rectangles: List[Rectangle], centers: List[Tuple[float, float]], radius: int):
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches

    fig = plt.figure()
    ax = fig.add_subplot(111, aspect='equal')

    for rect in rectangles:
        ax.add_patch(
            patches.Rectangle(
                (rect.x1, rect.y1),
                rect.x2 - rect.x1,
                rect.y2 - rect.y1,
                fill=False
            )
        )

    for center in centers:
        ax.add_patch(
            patches.Circle(
                center,
                radius,
                fill=False
            )
        )

    plt.xlim(0, 100)
    plt.ylim(0, 100)

    plt.show()

    return


def write_results(centers: List[Tuple[float, float]], extra: int = 0, radius: int = 0, seed: int = 0):
    index: int = 0
    while True:
        try:
            with open(str(extra) + "-" + str(radius) + "-" + str(index) + "-" + str(seed) + ".txt", 'r') as f:
                index += 1
        except FileNotFoundError:
            break

    with open(str(extra) + "-" + str(radius) + "-" + str(index) + "-" + str(seed) + ".txt", 'w') as f:
        f.write(str(len(centers)) + "\n")
        for i in centers:
            f.write(str(i[0]) + " " + str(i[1]) + "\n")

    return


print("Starting algorithm...")
print(len(rectangles))

start = time.time()
ret = algorithm(rectangles, tree, ids, angles, 500)
end = time.time()

print("Time:", end - start)

write_results(ret, extra_points, 500, seed)

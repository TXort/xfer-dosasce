#include <fstream>
#include <iostream>
#include <vector>

#include <boost/geometry.hpp>
#include <boost/geometry/index/rtree.hpp>

namespace Checker {

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<double, 2, bg::cs::cartesian> point;
typedef bg::model::box<point> box;
typedef std::pair<box, unsigned> value;

bgi::rtree<value, bgi::rstar<16>> rtree;

struct Point {
  double x;
  double y;
};

struct Rectangle {
  double x1;
  double y1;
  double x2;
  double y2;
};

int N;
double radius;
std::vector<Rectangle> data;

bool Intersects(Point &center, Rectangle &rect) {
  double width = rect.x2 - rect.x1;
  double height = rect.y2 - rect.y1;
  double circle_distance_x = std::abs(center.x - (rect.x1 + width / 2.0));
  double circle_distance_y = std::abs(center.y - (rect.y1 + height / 2.0));

  if (circle_distance_x > (width / 2 + radius)) {
    return false;
  }
  if (circle_distance_y > (height / 2 + radius)) {
    return false;
  }

  if (circle_distance_x <= (width / 2)) {
    return true;
  }
  if (circle_distance_y <= (height / 2)) {
    return true;
  }

  double corner_distance_sq = std::pow((circle_distance_x - width / 2), 2) +
                              std::pow((circle_distance_y - height / 2), 2);

  return (corner_distance_sq <= std::pow(radius, 2));
}

std::vector<value> FindPotentialMatches(Point &center) {
  box query_box(point(center.x - radius, center.y - radius),
                point(center.x + radius, center.y + radius));
  std::vector<value> potential_matches;
  rtree.query(bgi::intersects(query_box),
              std::back_inserter(potential_matches));

  return potential_matches;
}

std::vector<value> FilterMatches(Point &center,
                                 std::vector<value> &potential_matches) {
  std::vector<value> matches;
  for (const auto &match : potential_matches) {
    if (Intersects(center, data[match.second])) {
      matches.push_back(match);
    }
  }
  return matches;
}

void Check() {
  std::ifstream data_input("input.txt");

  if (!data_input.is_open()) {
    throw std::logic_error("Can't open test input");
  }

  data_input >> N >> radius;

  data.resize(N);
  for (int i = 0; i < N; ++i) {
    data_input >> data[i].x1 >> data[i].y1 >> data[i].x2 >> data[i].y2;
    rtree.insert(std::make_pair(
        box(point(data[i].x1, data[i].y1), point(data[i].x2, data[i].y2)), i));
  }
  data_input.close();

  std::ifstream user_input("points.txt");
  if (!user_input.is_open()) {
    throw std::logic_error("Can't open user input (output)");
  }

  int n_circles;
  user_input >> n_circles;

  for (int i = 0; i < n_circles; ++i) {
    double x, y;
    user_input >> x >> y;

    Point center{x, y};
    auto potential_matches = FindPotentialMatches(center);
    auto matches = FilterMatches(center, potential_matches);
    rtree.remove(matches);
  }

  // how many rectangles are left in the rtree?
  int left = 0;
    for (const auto &value : rtree) {
        ++left;
    }

  std::cout << "Preostalo " << left << " sarmi\n";

  if (!rtree.empty()) {
    std::cout << "Netocno rjesenje, grabljenja ne pokrivaju sve sarme\n";
  } else {
    std::cout << "Tocno rjesnje: " << N - n_circles << " bodova\n";
  }
}

} // namespace Checker

int main() { 
  Checker::Check(); 
}

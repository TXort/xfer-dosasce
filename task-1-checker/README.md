# Kako pokrenuti checker:

**Potrebno**: C++ compiler i C++ library Boost

Kako instalirati C++ Boost:

Ubuntu: \
*sudo apt-get install build-essential g++ python3-dev autotools-dev libicu-dev libbz2-dev libboost-all-dev*

Windows: \
*https://www.boost.org/doc/libs/1_83_0/more/getting_started/windows.html*

Kako compileati checker: \
otvorit terminal/cmd i upisati sljedeće naredbe \
`mkdir build` \
`cd build` \
`cmake -DCMAKE_BUILD_TYPE=Release ..` \
`make `\
`cd .. `\
`./build/src/Sarme` 

Ako koristite IDE poput VSCode-a možete instalirati CMake ekstenziju te kliknuti build u donjem lijevo kutu. Pripazite da odaberete Release build.

Checker radi tako da iz direktorija u kojem se trenutno nalazite učita datoteku "*input.txt*" koja sadrži ulazne podatke zadatka (podatke koje možete skinuti sa stranice) te zatim učita datoteku "*points.txt*" u kojoj se nalazi vaše rješenje.

Broj bodova se računa kao ukupan_broj_sarmi - broj_grabljenja (ako je broj grabljenja >= broju sarmi rješenje nosi 0 bodova)
